<?php

$DB_HOST = "localhost";
$DB_USER = "root";
$DB_NAME = "kasir";
$DB_PASS = "";
define('HOMEDIR', dirname(__FILE__));

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'IND');
spl_autoload_register(function($class) {
	if (is_file($class = HOMEDIR . '/incl/' . str_replace('_', '/', $class) . '.php')){
		require($class);
    }
});
new Page(array(
	'rw_base' => 'kasir',
        'lang_dir' => HOMEDIR.'/incl/bahasa',
        'lang_def' => 'id_ID',
	'db_dsn' => "mysql:host=$DB_HOST;dbname=$DB_NAME",
	'db_user' => $DB_USER,
	'db_pswd' => $DB_PASS
));
