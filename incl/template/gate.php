<?php
isset($this) || exit;

$title = "Kategori Menu";

ob_start();
?>
<p>
 Kategori menu digunakan untuk mengelompokkan menu yang disediakan mitra pada kategori tertentu. Misal kategori minuman,makanan,dll.
</p>
<div class="row">
 <div class="col-md-12">
  <table id="tabel" class="table table-bordered">
   <thead>
    <tr>
     <th>Nama</th>
     <th>Keterangan</th>
     <th width-="5%">Aksi</th>
    </tr>
   </thead>
   <tbody>
    <tr>
     <td>Makanan</td>
     <td>Minuman</td>
     <td>ddd</td>
    </tr>
   </tbody>
  </table>
 </div>
 <div class="col-md-12">
  <div class="form-group">
   <button data-toggle="modal" data-target="#modal-tambah" class="btn bg-aqua-active">Tambah</button>
  </div>
 </div>
 <?php
 General::html_modal_tambah();
 General::html_modal_edit();
 General::html_modal_hapus();
 ?>
</div>
<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/kategori';
     fetch_data('#tabel', base_url + "/read");

     $("#btn-tambah").click(function () {
         var data = $("#form-tambah").serialize();
         add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         add_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });

     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });

     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url+"/delete", data, '#modal-hapus', "#tabel");
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/gate_template.php';
?>
