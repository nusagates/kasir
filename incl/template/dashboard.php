<!doctype html>
<html>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/jquery.datetimepicker.css" />
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/rowReorder.dataTables.min.css">
  <script src="<?php echo $this->base_url . "/r/" ?>js/jQuery-2.1.4.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>js/jquery-ui.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>dist/js/app.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>js/jquery.dataTables.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>js/jquery.datetimepicker.full.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>js/dataTables.rowReorder.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>js/dataTables.responsive.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>js/jquery.responsiveTabs.min.js"></script>

  <title><?php echo!empty($title) ? $title : "Halaman Kosong" ?></title>
 </head>
 <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

   <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $this->base_url ?>" class="logo">
     <!-- mini logo for sidebar mini 50x50 pixels -->
     <span class="logo-mini"><b>E</b>Ass</span>
     <!-- logo for regular state and mobile devices -->
     <span class="logo-lg"><b>EMP</b>ASS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
     <!-- Sidebar toggle button-->
     <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
     </a>
     <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
       <!-- Notifications: style can be found in dropdown.less -->

       <!-- User Account: style can be found in dropdown.less -->
       <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
         <img src="<?php echo User::user_image_url($this->base_url) ?>" class="user-image" alt="User Image">
         <span class="hidden-xs">Admin</span>
        </a>
        <ul class="dropdown-menu">
         <!-- User image -->
         <li class="user-header">
          <img src="<?php echo User::user_image_url($this->base_url) ?>" class="img-circle" alt="User Image">
          <p>
              <?php echo User::display_name() ?>
           <small>Admin</small>
          </p>
         </li>
         <!-- Menu Footer-->
         <li class="user-footer">
          <div class="pull-left">
           <a href="#" class="btn btn-default btn-flat">Profile</a>
          </div>
          <div class="pull-right">
           <a href="<?php echo $this->base_url . "/gate/logout" ?>" class="btn btn-default btn-flat">Sign out</a>
          </div>
         </li>
        </ul>
       </li>
      </ul>
     </div>
    </nav>
   </header>
   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     <!-- Sidebar user panel -->
     <div class="user-panel">
      <div class="pull-left image">
       <img src="<?php echo User::user_image_url($this->base_url) ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
       <p><?php echo User::display_name() ?></p>
       <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
     </div>
     <!-- sidebar menu: : style can be found in sidebar.less -->
     <?php General::sidebar_menu($this) ?>
    </section>
    <!-- /.sidebar -->
   </aside>
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <h1>
         <?php echo!empty($title) ? $title : "Halaman Kosong" ?>
     </h1>
     <ol class="breadcrumb">
      <li><a href="<?php echo $this->base_url ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <?php
      $bc = explode("/", $this->req_uri);
      $bc_last = array_pop($bc) . "";
      foreach ($bc as $bc_sub) {
       echo '<li><a href="#">' . $bc_sub . '</a></li>';
      }
      if ("" !== $bc_last)
       echo '<li class="active">' . $bc_last . '</li>';
      ?>
     </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php
        echo $content;
        ?>
     <script src="<?php echo $this->base_url . "/r/" ?>js/functions.js"></script>
    </section><!-- /.content -->
   </div><!-- /.content-wrapper -->
   <footer class="main-footer">
    <div class="pull-right hidden-xs">
     <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; Nusagates Institute 2018</strong> All rights reserved.
   </footer>
  </div>
 </body>
</html>