<?php

class page_etc {

 public $ctx;

 function __construct($ctx) {

  $this->ctx = $ctx;
 }

 function index() {
  
 }

 function unAuthorized() {
  $this->ctx->_load_template($this, 'unauthorized_template');
 }

 function test() {
 if($this->ctx->isUserLoggedIn()){
  echo "1";
 }else{
  $this->ctx->_route('gate');
 }
 }

}
