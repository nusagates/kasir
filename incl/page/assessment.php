<?php

class page_assessment {

 public $ctx;
 private $table = 'criteria';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  } else {
   $this->ctx->_load_template($this, 'assessment');
  }
 }

 function read() {
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
 if(empty($search)){
  $query = $this->ctx->db->query("SELECT id, name, SUM(total) as total FROM(SELECT e.name as name, r.user as id, r.value * c.weight as total
FROM `reports` r 
LEFT JOIN employers e 
ON r.user=e.id
LEFT JOIN criteria c 
ON r.criteria=c.id
) q GROUP BY id ORDER by total DESC limit $start, $length");
 }else{
  $query = $this->ctx->db->prepare("SELECT id, name, SUM(total) as total FROM(SELECT e.name as name, r.user as id, r.value * c.weight as total
FROM `reports` r 
LEFT JOIN employers e 
ON r.user=e.id
LEFT JOIN criteria c 
ON r.criteria=c.id
) q where name like ? GROUP BY id ORDER by total DESC limit $start, $length");
  $query->execute(array("%".$search."%"));
 }
  $i =1;
  while($col=$query->fetchObject()){
   $menu =  "<a data-edit='" . $col->id . "' class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
           . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($i,$col->name,$col->total, $menu);
   $i++;
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post("name", $name))exit(Text('required', "Nama Karyawan"));
  General::s_post("tanggal", $tanggal);
  empty($tanggal)?$tanggal=date("Y-m-d H:i:s"):$tanggal;
  $db =  $this->ctx->db;
  $user = $db->prepare("insert into employers(name) values(?)");
  $info ="";
  if ($user->execute(array($name))) {
   $userId = $db->lastInsertId();
   $postCount = count($_POST);
   $i =1;
   $criteriaId=0;
   foreach ($_POST as $keys => $value) {
    if ($keys != "name" ) {
     if($keys!="tanggal"){
      $key = explode("-", $keys);
      $criteriaId = $key[1];
     }
     if (General::s_post($keys, $value))exit(Text('required', "Kriteria $key[0]"));
     break;
     $report = $db->prepare("insert into reports(tanggal,user, criteria, value) values(?, ?, ?, ?)");
     if($report->execute(array($tanggal,$userId, $criteriaId, $value))){
      if($i==$postCount-1){
       echo '1';
      }
     }else{
      if($i==$postCount-1){
       echo 'Penilaian gagal dibuat.';
      }
     }
     //echo $id . "=$value | ";
     $i++;
    }
    
   }
exit;
  }


  exit;
 }

 function edit_form() {
  if (General::s_post("id", $id))
   exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("SELECT  r.id, e.name, c.id criteria_id, c.name criteria_name, c.weight criteria_wheight, r.value FROM `reports` r 
LEFT JOIN employers e 
ON r.user=e.id
LEFT JOIN criteria c 
ON r.criteria=c.id WHERE r.user = ?");
  $query->execute(array($id));
  if ($query->rowCount()) {
   $i = 0;
   while ($data = $query->fetchObject()) {
    if ($i == 0) {
     echo "<div class='col-md-12'>Silahkan ubah data penilaian $data->name di bawah ini</div>";
     echo '<form id="form-edit" method="post">';
    }
    General::html_input($data->id, $data->criteria_name, 6, $data->value, 1, "number");
    $i++;
   }
   General::html_info();
   echo '</form>';
  }
 }

 function update() {
  $db = $this->ctx->db;
  $i=1;
  $total = count($_POST);
  foreach ($_POST as $key => $value) {
   if (empty($key) || empty($value)) {
    $cq = $db->prepare("select c.name from reports r left join criteria c on r.criteria=c.id where r.id=?");
    $cq->execute(array($key));
    if ($cq->rowCount()) {
     echo "1";
    }
   } else {
    $cq = $db->prepare("SELECT * FROM reports  where id=?");
    $cq->execute(array($key));
    if ($cq->rowCount()) {
     $update = $db->prepare("update reports set value=? where id=?");
     if($update->execute(array((int)$value, $key))){
      if($i===$total-1){
       echo "1";
      }
     }
    }
   }
   $i++;
  }
  exit;
 }

 function delete_form() {
  if (General::s_post("id", $id))
   exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from employers where id=?");
  $query->execute(array($id));
  if ($query->rowCount()) {
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus data penilaian milik <b class="text-red">' . $col->name . "</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id))
   exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("delete from reports where user=?");
  if ($query->execute(array($id))) {
   $deleteEmployer = $db->prepare("delete from employers where id=?");
   $deleteEmployer->execute(array($id));
   $count = $db->prepare("select id from reports");
   $count->execute();
   if ($count->rowCount() < 1) {
    $truncateReport = $db->query("TRUNCATE TABLE reports");
    $truncateReport->execute();
    $truncateEmployers = $db->query("TRUNCATE TABLE employers");
    $truncateEmployers->execute();
   }
   echo '1';
  }
 }

}
