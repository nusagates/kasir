<?php

class page_kategori {

 public $ctx;
 private $table = 'kategori';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'kategori');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("select * from $this->table order by name asc");
  }else{
   $query = $this->ctx->db->prepare("select * from $this->table where name like ? order by name asc");
   $query->execute(array("%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->name,$col->status, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post('name', $name))exit(text('required', 'Nama Kategori'));
  $db = $this->ctx->db;
  $query = $db->prepare("insert into $this->table(name,status) values(?,?)");
  if ($query->execute(array($name, "aktif"))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=? order by name asc");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("name", "Kriteria", 12, $col->name, 1);
   General::html_input("weight", "Bobot", 12, $col->weight);
   General::html_info();
   echo '</form>';
  }
 }

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('name', $name))exit(text('required', 'Kriteria'));
  General::s_post('weight', $weight);
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set name=?, weight=? where $this->id=?");
  if ($query->execute(array($name, $weight, $id))) {
   echo '1';
   exit;
  }
 }
 
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query1 = $this->ctx->db->prepare("select * from $this->table");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   if($query1->rowCount()==1){
    echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus <b class="text-red">'.$col->name."</b>? Tindakan ini juga akan menghapus semua data penilaian yang telah dibuat.</div>";
   }else{
    echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus <b class="text-red">'.$col->name."</b>?</div>";
   }
   
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("delete from $this->table where $this->id=?");
  if ($query->execute(array($id))) {
   $del = $db->prepare("delete from reports where criteria=?");
   if($del->execute(array($id))){
    $count = $db->query("select id as count from  criteria");
    $count->execute();
    if($count->rowCount()<1){
     $truncateEmployers = $db->query("TRUNCATE TABLE employers");
     $truncateEmployers->execute();
     $truncateCriteria = $db->query("TRUNCATE TABLE criteria");
     $truncateCriteria->execute();
     $truncateReport = $db->query("TRUNCATE TABLE reports");
     $truncateReport->execute();
    }
    echo "1";
   }
   exit;
  }
 }

}
