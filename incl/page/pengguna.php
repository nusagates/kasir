<?php

class page_pengguna {

 public $ctx;
 private $table = 'users';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'pengguna');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("select * from $this->table order by display_name asc");
  }else{
   $query = $this->ctx->db->prepare("select * from $this->table where display_name like ? order by display_name asc");
   $query->execute(array("%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->display_name,$col->user_group, $col->status, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post('name', $name))exit(text('required', 'Nama Lengkap'));
  if (General::s_post('login', $username))exit(text('required', 'Nama Pengguna'));
  if (General::s_post('password', $password))exit(text('required', 'Password'));
  if (General::s_post('group', $level))exit(text('required', 'Hak Akses'));
  $db = $this->ctx->db;
  $query = $db->prepare("insert into $this->table(display_name, username, password, user_group) values(?, ?, ?, ?)");
  if ($query->execute(array($name, $username, password_hash($password, PASSWORD_BCRYPT), $level))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=? limit 1");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
  
   General::html_input("name", "Nama Lengkap", 12, $col->display_name, 1);
   ?>
   <div class="col-md-12">
            <div class="form-group">
             <label for="group">Hak Akses</label>
             <select class="form-control" name="group" id="group">
              <option value="">Pilih hak akses...</option>
              <option <?php echo $col->user_group=="kasir"?"selected='selected'":"" ; ?> value="kasir">Kasir</option>
              <option <?php echo $col->user_group=="gudang"?"selected='selected'":"" ; ?> value="gudang">Admin Gudang</option>
              <option <?php echo $col->user_group=="admin"?"selected='selected'":"" ; ?> value="admin">Super Admin</option>
             </select>
            </div>
           </div>
<?php
   General::html_info();
   echo '</form>';
  }
 }

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('name', $name))exit(text('required', 'Nama Lengkap'));
  if (General::s_post('group', $level))exit(text('required', 'Hak Akses'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set display_name=?, user_group=? where id=?");
  if ($query->execute(array($name, $level, $id))) {
   echo '1';
   exit;
  }
 }
 
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus <b class="text-red">'.$col->display_name."</b>?</div>";
   
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("delete from $this->table where $this->id=?");
  if ($query->execute(array($id))) {
   exit("1");
  }
 }

}
