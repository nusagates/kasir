<?php

function text($key/*, argv, argv, ...*/) {
    return call_user_func_array(array(Page::$instance->text, "l"), func_get_args());
}

function _text($key/*, argv, argv, ...*/) {
    echo call_user_func_array(array(Page::$instance->text, "l"), func_get_args());
}

class Page {

	var $config;
	var $db;
	var $req_base;
	var $req_sub;
	var $req_uri;
	var $base_url;
    var $base_dir;
        
        var $text;

        static $instance;

	public function isUserLoggedIn() {
		return !empty($_SESSION['logged-in']);
	}

                
        public function __construct($config) {
                static::$instance = $this;

		$this->config = $config = (object) $config;
		session_start();

		# Koneksi database
		$this->db = new Pdo(
			$config->db_dsn, $config->db_user, $config->db_pswd);

                # Language setting
                $lang_id = 'id_ID';
                $this->text = new Text($config->lang_dir, $lang_id);

		# Parse request
		$base = 'etc';$sub = '_404';
		$request = strtolower(
			trim(preg_replace('!\/*(\?.*|#.*)$!', '', $_SERVER['REQUEST_URI']), '/'));
		if ("" !== $request) {
			if (!empty($config->rw_base))
				$request = preg_replace('!^\/*'.preg_quote($config->rw_base, '!').'(\/|$)!', '', $request)
			;

			if ('' !== $request) {
				if (preg_match('!^([a-z0-9]+(?:[\-][a-z0-9]+)*)((?:\/*[a-z0-9]+(?:[\-][a-z0-9]+)*)*)$!', $request, $part) && $part[1] !== 'etc') {				
					$sub	= ($part[2] = trim($part[2], '/')) !== '' ? $part[2] : 'index';
					$base = $part[1];
				}
			} else {$base = 'etc'; $sub = 'index';}
		} else {$base = 'etc'; $sub = !empty($config->rw_base) ? 'index' : '_404';}

		$this->base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . rtrim('/' . $config->rw_base, '/');
		$this->req_uri = $request;
		$this->_route($base, $sub);
	}

	function _route($base, $sub = 'index') {
		# Internal redirect
		if ($this->req_base === $base && $this->req_sub === $sub) {
			exit('Bad redirect');
		}

		if ($sub === 'index' && !class_exists('page_' . $base)) {
			$sub = $base;
			$base = 'etc';
		}

		$this->req_base = $base;
		$this->req_sub = $sub;

		# Kita tidak langsung menggunakan fungsi `method_exists` untuk mengecek,
		# agar klas bisa merubah variabel maupun konfigurasi (mis. ::req_sub) sebelum cek `method`
		# sehingga `sub request` bisa lebih dinamis
		if (class_exists($class = 'page_' . $base)) {
			$class = new $class($this);
			if (method_exists($class, $sub = str_replace('/', '_', $this->req_sub))) {
				$class->$sub();
				return;
			}
		}

		$this->_load_template($this, '404');
		exit;
	}

	function _load_template($context, $template) {
		if (is_file($template = HOMEDIR . '/incl/template/' . $template . '.php')) {
			return include($template);
		}
	}
}




